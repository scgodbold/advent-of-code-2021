package lib

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func ReadInput(path string) string {
	input, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err.Error())
	}
	return string(input)
}

func CastInt(input string) int {
    // We will be assuming input is "safe" since its a controlled environment, lets not do this
    // in real world use cases and actually check our errors

    i, _ := strconv.Atoi(strings.TrimSpace(input))
    return i
}

func IntRangeInclusive(start, end int) []int {
    values := []int{}
    if start < end {
        for i:=start; i<=end; i++ {
            values = append(values, i)
        }
    } else  {
        for i:=start; i>=end; i-- {
            values = append(values, i)
        }
    }
    return values
}

func WriteAnswer(day int, part int, answer string) {
    fmt.Printf("Day %v - Part %v: %v\n", day, part, answer)
}
