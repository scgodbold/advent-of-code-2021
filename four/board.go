package four

import (
	"log"
	"strconv"
	"strings"
)

type Cell struct {
	Value int
	Hit   bool
}

func (c *Cell) Check(val int) bool {
	if c.Hit {
		return false
	}
	if c.Value == val {
		c.Hit = true
		return true
	}
	return false

}

// 5 horz wins
// 5 vert wins
// 2 diag wins

type Board struct {
	Grid [][]*Cell
	Won  bool
}

func (b *Board) Fill(val int) bool {
	success := false
	for _, v := range b.Grid {
		for _, c := range v {
			if c.Check(val) {
				success = true
				break
			}
		}
		if success {
			break
		}
	}
	return success
}

func (b *Board) IsWinner() bool {
	if b.Won {
		return true
	}
	// Check rows
	for _, row := range b.Grid {
		rowComplete := true
		for _, cell := range row {
			if !cell.Hit {
				rowComplete = false
				break
			}
		}
		if rowComplete {
			b.Won = true
			break
		}
	}

	gridSize := len(b.Grid)

	// check cols
	for i := 0; i < gridSize; i++ {
		colComplete := true
		for j := 0; j < gridSize; j++ {
			if !b.Grid[j][i].Hit {
				colComplete = false
				break
			}
		}

		if colComplete {
			b.Won = true
			break
		}
	}

	return b.Won
}

func (b *Board) Score() int {
	total := 0
	for _, row := range b.Grid {
		for _, cell := range row {
			if !cell.Hit {
				total += cell.Value
			}
		}
	}
	return total
}

func NewBoard(input string) *Board {
	b := Board{
		Won:  false,
		Grid: [][]*Cell{},
	}
	for _, r := range strings.Split(input, "\n") {
		row := []*Cell{}
		if len(r) < 1 {
			continue
		}
		for _, cell := range strings.Fields(r) {
			i, err := strconv.Atoi(cell)
			if err != nil {
				i = 0
				log.Printf("Trouble converting value %v to an int\n", cell)
			}
			row = append(row, &Cell{Value: i, Hit: false})
		}
		b.Grid = append(b.Grid, row)
	}
	return &b
}
