package four

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"

	aocLib "gitlab.com/scgodbold/advent-of-code-2021/lib"
)

type Game struct {
	Boards  []*Board
	Numbers []int
	Step    int
}

func (g *Game) FindLosers() []*Board {
	losers := []*Board{}
	for _, b := range g.Boards {
		if b.IsWinner() {
			continue
		}
		losers = append(losers, b)
	}
	return losers
}

func (g *Game) FindWinners() []*Board {
	winners := []*Board{}
	for _, b := range g.Boards {
		if b.IsWinner() {
			winners = append(winners, b)
		}
	}
	return winners
}

func (g *Game) PullNumber() bool {
	if g.Step >= len(g.Numbers) {
		return false
	}
	number := g.Numbers[g.Step]
	for _, b := range g.Boards {
		b.Fill(number)
	}
	g.Step += 1
	return true
}

func (g *Game) FirstWinner() *Board {
	winners := []*Board{}
	for {
		if !g.PullNumber() {
			break
		}
		winners = g.FindWinners()
		if len(winners) > 0 {
			return winners[0]
		}
	}
	return &Board{}
}

func (g *Game) LastWinner() *Board {
	losers := []*Board{}
	for {
		if !g.PullNumber() {
			break
		}
		losers = g.FindLosers()
		if len(losers) == 1 {
			break
		}
	}
	if len(losers) != 1 {
		return &Board{}
	}

	loser := losers[0]
	for !loser.IsWinner() {
		g.PullNumber()
	}
	return loser
}

func (g *Game) LastNumberCalled() int {
	return g.Numbers[g.Step-1]
}

func NewGame(input string) Game {
	splits := strings.Split(input, "\n\n")
	game := Game{
		Boards:  []*Board{},
		Numbers: []int{},
		Step:    0,
	}

	// Populate numbers
	for _, v := range strings.Split(splits[0], ",") {
		i, err := strconv.Atoi(v)
		if err != nil {
			i = 0
			log.Printf("Trouble converting value %v to an int\n", v)
		}
		game.Numbers = append(game.Numbers, i)
	}

	// Populate our boards
	for _, v := range splits[1:] {
		board := NewBoard(v)
		game.Boards = append(game.Boards, board)
	}
	return game
}

func PartOne(path string, wg *sync.WaitGroup) {
    defer wg.Done()
	inputData := aocLib.ReadInput(path)
	game := NewGame(inputData)
	winner := game.FirstWinner()
    aocLib.WriteAnswer(4, 1, fmt.Sprintf("%v", winner.Score() * game.LastNumberCalled()))
}

func PartTwo(path string, wg *sync.WaitGroup) {
    defer wg.Done()
	inputData := aocLib.ReadInput(path)
	game := NewGame(inputData)
	loser := game.LastWinner()
    aocLib.WriteAnswer(4, 2, fmt.Sprintf("%v", loser.Score() * game.LastNumberCalled()))
}
