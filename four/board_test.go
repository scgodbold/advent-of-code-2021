package four

import (
	"testing"
)

func getBoard() *Board {
	input := "22 13 17 11 0\n8 2 23 4 24\n21  9 14 16  7\n6 10  3 18  5\n1 12 20 15 19"
	b := NewBoard(input)
	return b
}

func TestNewBoard(t *testing.T) {
	b := getBoard()
	// We arent going to check that these values are right, other tests can handle that, this will check
	// A. If its coming back as a success by default
	// B. If the size is as we expect (5x5)
	// C. If our cells are coming back as already hit

	if b.Won {
		t.Logf("Expected board to comeback as not won\n")
		t.Fail()
	}

	rows := len(b.Grid)

	if rows != 5 {
		t.Logf("Board came back poorly formated, Expected 5 rows, got: %v\n", rows)
		t.Fail()
	}

	for _, row := range b.Grid {
		cells := 0
		for _, cell := range row {
			cells += 1
			if cell.Hit {
				t.Logf("Cell came back poorly formated: Cell was already hit")
				t.Fail()
			}
		}

		if cells != 5 {
			t.Logf("Board came back poorly formated, Expected 5 cols, got: %v\n", cells)
			t.Fail()
		}
	}
}

func TestFillNumberSuccess(t *testing.T) {
	b := getBoard()
	c := b.Grid[4][4]

	b.Fill(c.Value)

	if !c.Hit {
		t.Logf("Expected hit on %v, but cell was not properly updated\n", c.Value)
		t.Fail()
	}
}

func TestFillNumberFailure(t *testing.T) {
	b := getBoard()
	b.Fill(1000000)

	hit := false
	for _, row := range b.Grid {
		for _, cell := range row {
			if cell.Hit {
				hit = true
				break
			}
		}
	}

	if hit {
		t.Logf("Cells where updated on a missed value\n")
		t.Fail()
	}
}

func TestFillNumberSuccessMultiple(t *testing.T) {
	b := getBoard()
	c := b.Grid[4][4]

	b.Fill(c.Value)
	b.Fill(c.Value)

	if !c.Hit {
		t.Logf("Expected hit on %v, but cell was not properly updated\n", c.Value)
		t.Fail()
	}
}

func TestIsWinnerFailure(t *testing.T) {
	b := getBoard()

	if b.IsWinner() {
		t.Logf("Board reported as a winner when no cells were updated\n")
		t.Fail()
	}
}

func TestIsWinnerRow(t *testing.T) {
	b := getBoard()
	row := b.Grid[3]
	for _, v := range row {
		v.Hit = true
	}

	if !b.IsWinner() {
		t.Logf("Board did not report as winner for successful row\n")
		t.Fail()
	}
}

func TestIsWinnerCol(t *testing.T) {
	b := getBoard()
	gridSize := len(b.Grid)
	for i := 0; i < gridSize; i++ {
		b.Grid[i][3].Hit = true
	}
	if !b.IsWinner() {
		t.Logf("Board did not report as winner for successful col\n")
		t.Fail()
	}
}

func TestCellCheckAlreadyHit(t *testing.T) {
	c := Cell{Value: 1, Hit: true}
	if c.Check(1) {
		t.Logf("Cell reported back as updated, despite alreadying being hit\n")
		t.Fail()
	}
}

func TestCellCheckHit(t *testing.T) {
	c := Cell{Value: 1, Hit: false}
	c.Check(1)
	if !c.Hit {
		t.Logf("Cell expected to be hit but wasnt\n")
		t.Fail()
	}
}

func TestCellCheckMiss(t *testing.T) {
	c := Cell{Value: 1, Hit: false}
	c.Check(2)
	if c.Hit {
		t.Logf("Cell expected to not be hit but was\n")
		t.Fail()
	}
}

func TestScore(t *testing.T) {
	b := getBoard()
	b.Fill(22)
	b.Fill(13)
	b.Fill(17)
	b.Fill(11)
	b.Fill(0)
	score := b.Score()

	if score != 237 {
		t.Logf("Score wasnt calculated properly, expected 237, got: %v", score)
		t.Fail()
	}
}
