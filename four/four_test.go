package four

import (
	"testing"
)

func getGame() Game {
	input := `7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7`

	return NewGame(input)
}

func TestSampleFirstWinner(t *testing.T) {
	g := getGame()
	winner := g.FirstWinner()
	if winner != g.Boards[2] {
		t.Logf("Expected board 3 to be winner but got a different baord\n")
		t.Fail()
	}
}

func TestGamePullNumber(t *testing.T) {
	g := getGame()
	g.PullNumber()

	if !g.Boards[0].Grid[2][4].Hit {
		t.Logf("Expected board one cell (Row:3, Col: 5) to be marked")
		t.Fail()
	}
	if !g.Boards[1].Grid[2][2].Hit {
		t.Logf("Expected board two cell (Row:3, Col: 3) to be marked")
		t.Fail()
	}
	if !g.Boards[2].Grid[4][4].Hit {
		t.Logf("Expected board three cell (Row:5, Col: 5) to be marked")
		t.Fail()
	}
}

func TestLastWinner(t *testing.T) {
	g := getGame()
	loser := g.LastWinner()
	if loser != g.Boards[1] {
		t.Logf("Expected board 2 to be loser but got a different baord\n")
		t.Fail()
	}
	if !loser.IsWinner() {
		t.Logf("Expected the last winner to be completed")
		t.Fail()
	}
}
