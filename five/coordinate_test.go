package five

import (
    "testing"
)


func TestNewCoordinate(t *testing.T) {
     c := NewCoordinate("1,2")
    if c.X != 1 && c.Y != 2 {
        t.Logf("Coordinate was not well formed, expected (1,2) got (%v, %v)", c.X, c.Y)
        t.Fail()
    }
}

func TestHorizontalLine(t *testing.T) {
    l := Line{
        Start: &Coordinate{X:1,Y:9},
        End: &Coordinate{X:4, Y:9},
    }

    points := l.horizontalLine()
    if len(points) != 4 {
        t.Logf("Expected 4 points in the horizontal line but only got: %v\n", len(points))
        t.FailNow()
    }
    for i, v := range []int{1,2,3,4} {
        if points[i].X != v {
            t.Logf("Expected point(%v, 9), got point (%v, %v)\n", v, points[i].X, points[i].Y)
            t.Fail()
        }
    }
}

func TestHorizontalLineDecending(t *testing.T) {
    l := Line{
        Start: &Coordinate{X:4,Y:9},
        End: &Coordinate{X:1, Y:9},
    }

    points := l.horizontalLine()
    if len(points) != 4 {
        t.Logf("Expected 4 points in the horizontal line but only got: %v\n", len(points))
        t.FailNow()
    }
    for i, v := range []int{4,3,2,1} {
        if points[i].X != v {
            t.Logf("Expected point(%v, 9), got point (%v, %v)\n", v, points[i].X, points[i].Y)
            t.Fail()
        }
    }
}

func TestVerticalLine(t *testing.T) {
    l := Line{
        Start: &Coordinate{X:9,Y:1},
        End: &Coordinate{X:9, Y:4},
    }

    points := l.verticalLine()
    if len(points) != 4 {
        t.Logf("Expected 4 points in the horizontal line but only got: %v\n", len(points))
        t.FailNow()
    }
    for i, v := range []int{1,2,3,4} {
        if points[i].Y != v {
            t.Logf("Expected point(9, %v), got point (%v, %v)\n", v, points[i].X, points[i].Y)
            t.Fail()
        }
    }
}

func TestVerticalLineDecending(t *testing.T) {
    l := Line{
        Start: &Coordinate{X:9,Y:4},
        End: &Coordinate{X:9, Y:1},
    }

    points := l.verticalLine()
    if len(points) != 4 {
        t.Logf("Expected 4 points in the horizontal line but only got: %v\n", len(points))
        t.FailNow()
    }
    for i, v := range []int{4,3,2,1} {
        if points[i].Y != v {
            t.Logf("Expected point(9, %v), got point (%v, %v)\n", v, points[i].X, points[i].Y)
            t.Fail()
        }
    }
}

func TestNewLine(t *testing.T) {
    l := NewLine("0,9 -> 5,9")
    if l.Start.ToString() != "0,9" {
        t.Logf("Expected to get starting coordinate of (0,9), but got (%v)\n", l.Start.ToString())
        t.Fail()
    }
    if l.End.ToString() != "5,9" {
        t.Logf("Expected to get starting coordinate of (5,9), but got (%v)\n", l.Start.ToString())
        t.Fail()
    }
}

func TestDiagnoalAllIncreasing(t *testing.T) {
    l := NewLine("7,9 -> 9,11")
    points := l.diagonalLine()
    if len(points) != 3 {
        t.Logf("Expected 3 points, got: %v", len(points))
        t.FailNow()
    }
    for i, v := range []string{"7,9", "8,10", "9,11"} {
        if points[i].ToString() != v {
            t.Logf("Expected point (%v) at position %v, got: (%v)", v, i, points[i].ToString())
            t.Fail()
        }
    }
}

func TestDiagnoalXIncreaseYDecrease(t *testing.T) {
    l := NewLine("7,11 -> 9,9")
    points := l.diagonalLine()
    if len(points) != 3 {
        t.Logf("Expected 3 points, got: %v", len(points))
        t.FailNow()
    }
    for i, v := range []string{"7,11", "8,10", "9,9"} {
        if points[i].ToString() != v {
            t.Logf("Expected point (%v) at position %v, got: (%v)", v, i, points[i].ToString())
            t.Fail()
        }
    }
}

func TestDiagonalYIncreasingXDecreasing(t *testing.T) {
    l := NewLine("9,9 -> 7,11")
    points := l.diagonalLine()
    if len(points) != 3 {
        t.Logf("Expected 3 points, got: %v", len(points))
        t.FailNow()
    }
    for i, v := range []string{"9,9", "8,10", "7,11"} {
        if points[i].ToString() != v {
            t.Logf("Expected point (%v) at position %v, got: (%v)", v, i, points[i].ToString())
            t.Fail()
        }
    }
}

func TestDiagonalDecreasingAll(t *testing.T) {
    l := NewLine("9,11 -> 7,9")
    points := l.diagonalLine()
    if len(points) != 3 {
        t.Logf("Expected 3 points, got: %v", len(points))
        t.FailNow()
    }
    for i, v := range []string{"9,11", "8,10", "7,9"} {
        if points[i].ToString() != v {
            t.Logf("Expected point (%v) at position %v, got: (%v)", v, i, points[i].ToString())
            t.Fail()
        }
    }
}
