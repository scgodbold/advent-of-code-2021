package five

import (
	"fmt"
	"strings"
	"sync"

	aocLib "gitlab.com/scgodbold/advent-of-code-2021/lib"
)

type Diagram struct {
    Vents []*Line
}

func (d *Diagram) dangerMap() map[string]int {
    m := map[string]int{}
    for _, line := range d.Vents {
        for _, point := range line.Points() {
            _, ok := m[point.ToString()]
            if !ok {
                m[point.ToString()] = 1
            } else {
                m[point.ToString()] += 1
            }
        }
    }
    return m
}

func (d *Diagram) simpleDangerMap() map[string]int {
    m := map[string]int{}
    for _, line := range d.Vents {
        for _, point := range line.SimplePoints() {
            _, ok := m[point.ToString()]
            if !ok {
                m[point.ToString()] = 1
            } else {
                m[point.ToString()] += 1
            }
        }
    }
    return m
}

func (d *Diagram) SimpleHighDangerCount() int {
    danger := 0
    dangerMap := d.simpleDangerMap()
    for _, v := range dangerMap {
        if v >= 2 {
            danger += 1
        }
    }
    return danger
}

func (d *Diagram) HighDangerCount() int {
    danger := 0
    dangerMap := d.dangerMap()
    for _, v := range dangerMap {
        if v >= 2 {
            danger += 1
        }
    }
    return danger
}

func NewDiagram(input string) *Diagram{
    d := Diagram{}
    for _, v := range strings.Split(input, "\n") {
        if v == "" {
            continue
        }
        d.Vents = append(d.Vents, NewLine(v))
    }
    return &d
}


func PartOne(path string, wg *sync.WaitGroup) {
    defer wg.Done()
    input := aocLib.ReadInput(path)
    diagram := NewDiagram(input)
    aocLib.WriteAnswer(5, 1, fmt.Sprintf("%v", diagram.SimpleHighDangerCount()))
}

func PartTwo(path string, wg *sync.WaitGroup) {
    defer wg.Done()
    input := aocLib.ReadInput(path)
    diagram := NewDiagram(input)
    aocLib.WriteAnswer(5, 2, fmt.Sprintf("%v", diagram.HighDangerCount()))
}
