package five

import (
    "testing"
)

func getSample() string {
    input := `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
`
    return input
}

func TestSampleInputSimple(t *testing.T) {
    d := NewDiagram(getSample())
    if d.SimpleHighDangerCount() != 5 {
        t.Logf("Expected 5 high danger points, got %v",d.SimpleHighDangerCount())
        t.Fail()
    }
}

func TestSampleInput(t *testing.T) {
    d := NewDiagram(getSample())
    if d.HighDangerCount() != 12 {
        t.Logf("Expected 12 high danger points, got %v",d.HighDangerCount())
        t.Fail()
    }
}
