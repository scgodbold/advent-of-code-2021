package five

import (
    "fmt"
    "math"
	aocLib "gitlab.com/scgodbold/advent-of-code-2021/lib"
    "strings"
)

type Coordinate struct {
    X int
    Y int
}

func NewCoordinate(input string) *Coordinate {
    split := strings.Split(input, ",")
    x := aocLib.CastInt(split[0])
    y := aocLib.CastInt(split[1])
    return &Coordinate{
        X: x,
        Y: y,
    }
}

func (c *Coordinate) ToString() string {
    return fmt.Sprintf("%v,%v", c.X, c.Y)
}

type Line struct {
    Start *Coordinate
    End *Coordinate
}

func (l *Line) SimplePoints() []Coordinate {
    if l.Start.Y == l.End.Y {
        return l.horizontalLine()
    } else if l.Start.X == l.End.X {
        return l.verticalLine()
    }
    return []Coordinate{}
}

func (l *Line) Points() []Coordinate {
    if l.Start.Y == l.End.Y {
        return l.horizontalLine()
    } else if l.Start.X == l.End.X {
        return l.verticalLine()
    } else {
        return l.diagonalLine()
    }
}

func (l *Line) horizontalLine() []Coordinate{
    coords := []Coordinate{}
    for _, v := range aocLib.IntRangeInclusive(l.Start.X, l.End.X) {
        coords = append(coords, Coordinate{X: v, Y: l.Start.Y})
    }
    return coords
}

func (l *Line) verticalLine() []Coordinate{
    coords := []Coordinate{}
    for _, v := range aocLib.IntRangeInclusive(l.Start.Y, l.End.Y) {
        coords = append(coords, Coordinate{X: l.Start.X, Y: v})
    }
    return coords
}

func (l *Line) diagonalLine() []Coordinate{
    coords := []Coordinate{}
    xSteps := int(math.Abs(float64(l.Start.X - l.End.X)))
    ySteps := int(math.Abs(float64(l.Start.Y - l.End.Y)))
    if xSteps != ySteps {
        return coords
    }
    xPoints := aocLib.IntRangeInclusive(l.Start.X, l.End.X)
    yPoints := aocLib.IntRangeInclusive(l.Start.Y, l.End.Y)
    for i, v := range xPoints {
        coords = append(coords, Coordinate{X: v, Y: yPoints[i]})
    }
    return coords
}

func NewLine(input string) *Line {
    split := strings.Split(input, " -> ")
    line := Line{
        Start: NewCoordinate(split[0]),
        End: NewCoordinate(split[1]),
    }
    return &line
}
