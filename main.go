package main

import (
	// "fmt"
    "sync"

	"gitlab.com/scgodbold/advent-of-code-2021/six"
	"gitlab.com/scgodbold/advent-of-code-2021/five"
    "gitlab.com/scgodbold/advent-of-code-2021/four"
	"gitlab.com/scgodbold/advent-of-code-2021/one"
	"gitlab.com/scgodbold/advent-of-code-2021/three"
	"gitlab.com/scgodbold/advent-of-code-2021/two"
)

func main() {
    var wg sync.WaitGroup

	// Day One
    wg.Add(2)
    go one.PartOne("inputs/one.txt", &wg)
    go one.PartTwo("inputs/one.txt", &wg)

	// Day Two
    wg.Add(2)
    go two.PartOne("inputs/two.txt", &wg)
    go two.PartTwo("inputs/two.txt", &wg)

	// Day Three
    wg.Add(2)
    go three.PartOne("inputs/three.txt", &wg)
    go three.PartTwo("inputs/three.txt", &wg)

	// Day Four
    wg.Add(2)
    go four.PartOne("inputs/four.txt", &wg)
    go four.PartTwo("inputs/four.txt", &wg)

	// Day Five
    wg.Add(2)
    go five.PartOne("inputs/five.txt", &wg)
    go five.PartTwo("inputs/five.txt", &wg)

	// Day Six
    wg.Add(2)
    go six.PartOne("inputs/six.txt", &wg)
    go six.PartTwo("inputs/six.txt", &wg)

    // Wait for completion
    wg.Wait()
}
