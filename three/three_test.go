package three

import (
	"testing"
)

func getSampleDiag() Diagnostic {
	d := Diagnostic{
		Input: []string{
			"00100",
			"11110",
			"10110",
			"10111",
			"10101",
			"01111",
			"00111",
			"11100",
			"10000",
			"11001",
			"00010",
			"01010",
		},
	}
	return d
}

func TestDiagnosticGamma(t *testing.T) {
	d := getSampleDiag()

	if d.Gamma() != 22 {
		t.Logf("Error expected Gamma value of 22, got: %v\n", d.Gamma())
		t.Fail()
	}
}

func TestDiagnosticEpsilon(t *testing.T) {
	d := getSampleDiag()

	if d.Epsilon() != 9 {
		t.Logf("Error expected Gamma value of 9, got: %v\n", d.Epsilon())
		t.Fail()
	}
}

func TestDiagnosticO2(t *testing.T) {
	d := getSampleDiag()

	if d.O2GenRating() != 23 {
		t.Logf("Error expected an O2Reading value of 23, got: %v\n", d.O2GenRating())
		t.Fail()
	}
}

func TestDiagnosticCO2(t *testing.T) {
	d := getSampleDiag()

	if d.CO2ScruberRating() != 10 {
		t.Logf("Error expected an O2Reading value of 10, got: %v\n", d.CO2ScruberRating())
		t.Fail()
	}
}
