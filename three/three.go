package three

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"

	aocLib "gitlab.com/scgodbold/advent-of-code-2021/lib"
)

type BitCounter struct {
	ZeroCount []int
	OneCount  []int
}

func (c *BitCounter) CommonBits() string {
	common := []string{}
	for i, _ := range c.ZeroCount {
		if c.ZeroCount[i] > c.OneCount[i] {
			common = append(common, "0")
		} else {
			common = append(common, "1")
		}
	}
	return strings.Join(common, "")
}

func (c *BitCounter) RareBits() string {
	common := []string{}
	for i, _ := range c.ZeroCount {
		if c.OneCount[i] < c.ZeroCount[i] {
			common = append(common, "1")
		} else {
			common = append(common, "0")
		}
	}
	return strings.Join(common, "")
}

func NewBitCounter(input []string) BitCounter {
	// initialize
	counter := BitCounter{
		ZeroCount: make([]int, len(input[0])),
		OneCount:  make([]int, len(input[0])),
	}
	for i, _ := range input[0] {
		counter.ZeroCount[i] = 0
		counter.OneCount[i] = 0
	}

	// Calculate
	for _, v := range input {
		for i, bit := range v {
			switch bit {
			case '0':
				counter.ZeroCount[i] += 1
			case '1':
				counter.OneCount[i] += 1
			}
		}
	}

	return counter
}

// Util Function
func BinaryToBaseTen(bin string) int64 {
	out, err := strconv.ParseInt(bin, 2, 64)
	if err != nil {
		log.Printf("Unable to convert %v to base 10\n", bin)
		out = 0
	}
	return out
}

type Diagnostic struct {
	Input []string
}

func (d *Diagnostic) Gamma() int64 {
	counter := NewBitCounter(d.Input)
	bits := counter.CommonBits()
	return BinaryToBaseTen(bits)
}

func (d *Diagnostic) Epsilon() int64 {
	counter := NewBitCounter(d.Input)
	bits := counter.RareBits()
	return BinaryToBaseTen(bits)
}

func (d *Diagnostic) O2GenRating() int64 {
	var choices []string
	keep := make([]string, len(d.Input))
	copy(keep, d.Input)
	for i := 0; i < len(d.Input[0]); i++ {
		// We do a reset
		choices = make([]string, len(keep))
		copy(choices, keep)
		keep = []string{}

		// Set our counter & bits
		counter := NewBitCounter(choices)
		bits := counter.CommonBits()

		// Do our matches
		for _, v := range choices {
			if v[i] == bits[i] {
				keep = append(keep, v)
			}
		}
	}

	return BinaryToBaseTen(keep[0])
}

func (d *Diagnostic) CO2ScruberRating() int64 {
	var choices []string
	keep := make([]string, len(d.Input))
	copy(keep, d.Input)
	for i := 0; i < len(d.Input[0]); i++ {
		// We do a reset
		choices = make([]string, len(keep))
		copy(choices, keep)
		keep = []string{}

		// Set our counter & bits
		counter := NewBitCounter(choices)
		bits := counter.RareBits()

		// Do our matches
		for _, v := range choices {
			if v[i] == bits[i] {
				keep = append(keep, v)
			}
		}

		if len(keep) == 1 {
			break
		}
	}
	return BinaryToBaseTen(keep[0])
}

func NewDiagnostic(path string) Diagnostic {
	diag := Diagnostic{
		Input: []string{},
	}
	for _, v := range strings.Split(aocLib.ReadInput(path), "\n") {
		if v == "" {
			continue
		}
		diag.Input = append(diag.Input, v)
	}
	return diag
}

func PartOne(path string, wg *sync.WaitGroup) {
    defer wg.Done()
	diag := NewDiagnostic(path)
    aocLib.WriteAnswer(3, 1, fmt.Sprintf("%v", diag.Gamma() * diag.Epsilon()))
}

func PartTwo(path string, wg *sync.WaitGroup) {
    defer wg.Done()
	diag := NewDiagnostic(path)
	aocLib.WriteAnswer(3, 2, fmt.Sprintf("%v", diag.O2GenRating() * diag.CO2ScruberRating()))
}
