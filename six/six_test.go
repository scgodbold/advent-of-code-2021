package six

import (
    "testing"
)

func TestNewSchool(t *testing.T) {
    s := NewSchool("3,4,3,1,2")
    if s.Size() != 5 {
        t.Logf("Expected a school with 5 fish got one with %v fish", s.Size())
        t.Fail()
    }
}


func TestAgeOne(t *testing.T) {
    s := NewSchool("3,2,1,0")
    s.AgeOne()
    if s.Size()!= 5 {
        t.Log(s.Fish)
        t.Logf("Expected a 5 fish in the school, had %v", s.Size())
        t.Fail()
    }
}

func TestAge(t *testing.T) {
    s := NewSchool("3,4,3,1,2")
    s.Age(80)

    if s.Size() != 5934 {
        t.Logf("Expected a 5934 fish in the school, had %v", s.Size())
        t.Fail()
    }
}

func TestAgeShort(t *testing.T) {
    days := 18
    size := 26
    s := NewSchool("3,4,3,1,2")
    s.Age(days)

    if s.Size() != size {
        t.Logf("Expected a %v fish in the school, had %v", size, s.Size())
        t.Fail()
    }
}

func TestAgeLong(t *testing.T) {
    s := NewSchool("3,4,3,1,2")
    s.Age(256)

    if s.Size() != 26984457539 {
        t.Logf("Expected a 26984457539 fish in the school, had %v", s.Size())
        t.Fail()
    }
}
