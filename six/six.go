package six

import (
    aoc "gitlab.com/scgodbold/advent-of-code-2021/lib"

    "fmt"
    "strings"
    "sync"
)

func emptySchool() map[int]int {
    return map[int]int{
            0: 0,
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
            7: 0,
            8: 0,
    }
}

type School struct {
    Fish map[int]int
}

func (s *School) Size() int {
    sum := 0
    for _, v := range s.Fish {
        sum += v
    }
    return sum
}

func (s *School) AgeOne() {
    newFish := emptySchool()
    for k, v := range s.Fish {
        if k == 0 {
            newFish[8] += v
            newFish[6] += v
        } else {
            newFish[k-1] += v
        }
    }
    for k, v := range newFish {
        s.Fish[k] = v
    }
}

func (s *School) Age(days int) {
    for i:=0; i<days; i++ {
        s.AgeOne()
    }
}

func NewSchool(input string) *School {
    school := School{
        Fish: emptySchool(),
    }

    for _, v := range strings.Split(input, ",") {
        age := aoc.CastInt(v)
        school.Fish[age] += 1
    }
    return &school
}

func PartOne(input string, wg *sync.WaitGroup) {
    defer wg.Done()
    inputData := aoc.ReadInput(input)
    school := NewSchool(inputData)
    school.Age(80)
    aoc.WriteAnswer(6, 1, fmt.Sprintf("%v", school.Size()))
}

func PartTwo(input string, wg *sync.WaitGroup) {
    defer wg.Done()
    inputData := aoc.ReadInput(input)
    school := NewSchool(inputData)
    school.Age(256)
    aoc.WriteAnswer(6, 2, fmt.Sprintf("%v", school.Size()))
}
