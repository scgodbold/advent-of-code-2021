package two

import (
	"testing"
)

func getSampleComputation() Computation {
	c := Computation{
		Position: Location{Horizontal: 0, Depth: 0, Aim: 0},
		Instructions: []Instruction{
			Instruction{Action: "forward", Magnitude: 5},
			Instruction{Action: "down", Magnitude: 5},
			Instruction{Action: "forward", Magnitude: 8},
			Instruction{Action: "up", Magnitude: 3},
			Instruction{Action: "down", Magnitude: 8},
			Instruction{Action: "forward", Magnitude: 2},
		},
	}
	return c
}

func TestSampleInputCoordinatesSimple(t *testing.T) {
	c := getSampleComputation()
	c.ProcessSimple()

	if c.Position.Depth != 10 || c.Position.Horizontal != 15 {
		t.Logf("Error expected (D: 10, H: 15), got: (D: %v: H: %v)", c.Position.Depth, c.Position.Horizontal)
		t.Fail()
	}

}

func TestSampleInputCoordinatesComplex(t *testing.T) {
	c := getSampleComputation()
	c.ProcessComplex()

	if c.Position.Depth != 60 || c.Position.Horizontal != 15 {
		t.Logf("Error expected (D: 60, H: 15), got: (D: %v: H: %v)", c.Position.Depth, c.Position.Horizontal)
		t.Fail()
	}

}
