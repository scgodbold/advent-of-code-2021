package two

import (
	aocLib "gitlab.com/scgodbold/advent-of-code-2021/lib"
    "fmt"
	"log"
	"strconv"
	"strings"
    "sync"
)

type Computation struct {
	Position     Location
	Instructions []Instruction
}

type Location struct {
	Horizontal int
	Depth      int
	Aim        int
}

type Instruction struct {
	Action    string
	Magnitude int
}

func (c *Computation) ProcessSimple() {
	for _, v := range c.Instructions {
		switch v.Action {
		case "forward":
			c.Position.Horizontal += v.Magnitude
		case "down":
			c.Position.Depth += v.Magnitude
		case "up":
			c.Position.Depth -= v.Magnitude
		default:
			log.Printf("Unable to process action type %v\n", v.Action)
		}
	}
}

func (c *Computation) ProcessComplex() {
	for _, v := range c.Instructions {
		switch v.Action {
		case "forward":
			c.Position.Horizontal += v.Magnitude
			c.Position.Depth += (c.Position.Aim * v.Magnitude)
		case "down":
			c.Position.Aim += v.Magnitude
		case "up":
			c.Position.Aim -= v.Magnitude
		default:
			log.Printf("Unable to process action type %v\n", v.Action)
		}
	}
}

func NewComputation(path string) Computation {
	computation := Computation{
		Position:     Location{Horizontal: 0, Depth: 0, Aim: 0},
		Instructions: []Instruction{},
	}
	input := aocLib.ReadInput(path)
	for _, v := range strings.Split(input, "\n") {
		if v == "" {
			continue
		}

		split := strings.Split(v, " ")
		if len(split) != 2 {
			log.Printf("Unexpected line skipping: %v\n", v)
			continue
		}
		action := split[0]
		magnitude, err := strconv.Atoi(split[1])
		if err != nil {
			log.Printf("Unable to expand %v to an int, skipping line: %v\n", split[1], v)
		}

		instruction := Instruction{
			Action:    action,
			Magnitude: magnitude,
		}
		computation.Instructions = append(computation.Instructions, instruction)
	}

	return computation
}

func PartOne(path string, wg *sync.WaitGroup) {
    defer wg.Done()
	computation := NewComputation(path)
	computation.ProcessSimple()
	aocLib.WriteAnswer(2,1,fmt.Sprintf("%v", computation.Position.Horizontal * computation.Position.Depth))
}

func PartTwo(path string, wg *sync.WaitGroup) {
    defer wg.Done()
	computation := NewComputation(path)
	computation.ProcessComplex()
	aocLib.WriteAnswer(2,2,fmt.Sprintf("%v", computation.Position.Horizontal * computation.Position.Depth))
}
