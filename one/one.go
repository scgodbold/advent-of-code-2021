package one

import (
	"log"
	"strconv"
	"strings"
    "fmt"
	"sync"

	aocLib "gitlab.com/scgodbold/advent-of-code-2021/lib"
)

type Readings struct {
	Data []int
}

func newReading(input string) (Readings, error) {
	readings := Readings{
		Data: []int{},
	}
	inputString := aocLib.ReadInput(input)
	for _, v := range strings.Split(string(inputString), "\n") {
		if v == "" {
			continue
		}
		i, err := strconv.Atoi(v)
		if err != nil {
			log.Printf("Unable to convert `%v` to an int", v)
			continue
		}

		readings.Data = append(readings.Data, i)

	}
	return readings, nil
}

func (r *Readings) IncreasingWindows(size int) int {
	count := 0
	for i, _ := range r.Data {
		if i+size+1 > len(r.Data) {
			break
		}
		sliceA := r.Data[i : i+size]
		sliceB := r.Data[i+1 : i+size+1]
		sumA := sum(sliceA)
		sumB := sum(sliceB)
		if sumB > sumA {
			count += 1
		}
	}
	return count
}

func sum(input []int) int {
	total := 0
	for _, v := range input {
		total += v
	}
	return total
}

// func main() {
// 	log.Println("Initializing")
// 	data, err := populateInput()
// 	if err != nil {
// 		log.Fatal(err.Error())
// 	}
// 	fmt.Printf("Part A Answer: %v\n", data.CountIncreasing())
// 	fmt.Printf("Part B Answer: %v\n", data.IncreasingWindows(3))
// }

func PartOne(input string, wg *sync.WaitGroup) {
    defer wg.Done()

	data, err := newReading(input)
	if err != nil {
		log.Println(err.Error())
		return
	}

    aocLib.WriteAnswer(1, 1, fmt.Sprintf("%v", data.IncreasingWindows(1)))
}

func PartTwo(input string, wg *sync.WaitGroup) {
    defer wg.Done()
	data, err := newReading(input)
	if err != nil {
		log.Println(err.Error())
		return
	}
    aocLib.WriteAnswer(1, 2, fmt.Sprintf("%v", data.IncreasingWindows(3)))
}
