package one

import (
	"testing"
)

func TestCountIncreasing(t *testing.T) {
	r := Readings{
		Data: []int{1, 2, 3, 4},
	}
	expected := 3

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestCountIncreasingExample(t *testing.T) {
	r := Readings{
		Data: []int{199, 200, 208, 210, 200, 207, 240, 269, 260, 263},
	}
	expected := 7

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestCountIncreasingEmpty(t *testing.T) {
	r := Readings{
		Data: []int{},
	}
	expected := 0

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestCountIncreasingOne(t *testing.T) {
	r := Readings{
		Data: []int{1},
	}
	expected := 0

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestCountIncreasingTwo(t *testing.T) {
	r := Readings{
		Data: []int{1, 2},
	}
	expected := 1

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestCountIncreasingDecreasing(t *testing.T) {
	r := Readings{
		Data: []int{3, 2, 1, 0},
	}
	expected := 0

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestCountIncreasingDecreaseOne(t *testing.T) {
	r := Readings{
		Data: []int{0, 1, 2, 1, 3},
	}
	expected := 3

	result := r.IncreasingWindows(1)

	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestSumSingle(t *testing.T) {
	expect := 5
	result := sum([]int{5})

	if expect != result {
		t.Logf("Error, should be %v, but got %v", expect, result)
		t.Fail()
	}
}

func TestSumEmpty(t *testing.T) {
	expect := 0
	result := sum([]int{})

	if expect != result {
		t.Logf("Error, should be %v, but got %v", expect, result)
		t.Fail()
	}
}

func TestSumMany(t *testing.T) {
	expect := 6
	result := sum([]int{1, 2, 3})

	if expect != result {
		t.Logf("Error, should be %v, but got %v", expect, result)
		t.Fail()
	}
}

func TestIncreasingWindowsProvided(t *testing.T) {
	r := Readings{
		Data: []int{199, 200, 208, 210, 200, 207, 240, 269, 260, 263},
	}
	expected := 5
	window := 3

	result := r.IncreasingWindows(window)
	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestIncreasingWindowsLargerThanSlice(t *testing.T) {
	r := Readings{
		Data: []int{1},
	}
	expected := 0
	window := 3

	result := r.IncreasingWindows(window)
	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}

func TestIncreasingWindowsWindowOne(t *testing.T) {
	r := Readings{
		Data: []int{1, 2, 3},
	}
	expected := 2
	window := 1

	result := r.IncreasingWindows(window)
	if result != expected {
		t.Logf("Error should be %v, but got %v", expected, result)
		t.Fail()
	}
}
